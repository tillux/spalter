# spalter

spalter is a pseudo-supervised machine learning approach to distinguish between technical artefacts and true variants in DNA-sequencing data.
It assumes that true variants appear independently of technical features of the reads they appear in (i.e. base quality, read strand).

## Getting Started
You can either build spalter from source or install it via conda.

### Installation
#### Installation from source
##### Prerequisites

To build spalter from source, rust >= `1.26.0` is required.
We recommend installing rust and cargo using the [rustup](https://rustup.rs/) script.

```
curl https://sh.rustup.rs -sSf | sh
```
Once the rust toolchain has been installed, clone the spalter repository, build the release version and run `cargo install` to install locally (or create a symlink to the binary if you do not wish to clutter your PATH):
```
git clone git@bitbucket.org:tillux/spalter.git
cd spalter
cargo build --release
cargo install  # or: ln -s target/release/spalter .
```

#### Installation via conda
Create an environment called spalter and install spalter from the bioconda channel:
```
conda create -n spalter -c bioconda spalter
```

### Examples
The usual pattern of usage consists of

1. Computation of separability profiles:

    ```
    spalter features input.bam -t reference.fa -o input.sfts.gz
    ```

    * use `--iojobs NUM` to use multiple jobs for reading, best in combination with `--region-size SIZE`.

    * use `--jobs NUM` to adjust the number of worker threads *per IO job* (1 to 4 should be enough, depending on coverage).

2. (Optional) Fit a model

    ```
    spalter tune input.sfts.gz snps.vcf.gz -e exclude.vcf.gz -o input.model
    ```

    * use `--num-sites NUM` to restrict the number of sites used for training to a random subsample of size `NUM`.

    * use `--classifier [rforest,lsvm,rbfsvm,dtreeN,threshold]` to choose a classifier for training.

3. Call errors/variants

    ```
    spalter call input.sfts.gz -m input.model -o calls.vcf.gz
    ```

    * You can either call using the separability profiles from step 1 as shown here or specify a .bam file and reference (via `-t reference.fa`) instead.

    * In order to use a mean separability threshold to call errors, use `-m threshold:X` where `X` is a floating point number such as `0.8`.

    * If you use more than 1 iojob and 1 worker per iojob, any resulting file will not be ordered. Consider using the following pipe instead:

        ```
        spalter call input.sfts.gz -m input.model | vcf-sort -c | bgzip -c > lsvm.is_reverse.vcf.gz
        ```

## Usage
spalter has four subcommands:

1. `features` to compute separability profiles from a given BAM + reference FASTA.
2. `call` to call technical artefacts either from the features computed in step 1 or from a given BAM + reference FASTA.
3. `tune` to train a classification model given the features computed in step 1 and a VCF file[^1] specifying SNP sites (anything not a SNP site is deemed an error).
4. `guess` to guess a mean separability threshold from the features computed in step 1.

### `features` and `call`
Subcommands `features` and `call` share the following common flags and options:

```
FLAGS:
    -h, --help                  
            Prints help information
    -V, --version               
            Prints version information
        --progress              
            Show progress on stderr.
        --estimate-threshold    
            Estimate threshold for distinguishing between technical artefacts and SNPs.

OPTIONS:
    -t, --target <REFERENCE>           
            Target/reference file (.fasta/.fa/.fna). Must have an accompanying index (.fai).
    -e, --exclude <VCF>                
            Skip sites from `VCF`.
    -a, --min-alt-frac <NUM>           
            Require an alternate allele frequency of at least `NUM`. [default: 0.1]
    -d, --min-read-depth <NUM>         
            Require at least `NUM` reads in each pileup. [default: 10]
        --classifiers <CLASSIFIERS>    
            Use `CLASSIFIERS` for meta feature extraction. [default: dtree]  [possible values: lsvm, rbfsvm, dtree,
            rforest, sgd, fm]
    -f, --feature-fns <FEATURES>       
            Use the given list of comma separated features. [default:
            is_read1,is_reverse,neighbour_base_quality,end_proximity]  [possible values: is_read1, is_reverse,
            neighbour_base_quality, end_proximity]
    -i, --iojobs <NUM>                 
            Number of io worker processes. Useful if BAM and fasta files are indexed. The best choice for iojobs depends
            on hardware specifications. If output order is important, set both iojobs and jobs 1 (or sort the resulting
            VCF file in post processing).
             [default: 1]
    -j, --jobs <NUM>                   
            Number of worker processes *for each* io-worker. If 0, determine number of workers as follows: `num CPUs /
            iojobs + 1`. If output order is important, set both iojobs and jobs to 1 (or sort the resulting VCF file in
            post processing).
             [default: 0]
    -o, --output <PATH>                
            Output filename. If omitted, write to stdout.
    -r, --regions <REGION(S)>          
            A list of comma separated region specifiers `targetname:start-end`.
    -s, --region-size <NUM>            
            Sets maximum region size, i.e. larger regions will be split. Only noticeably relevant if iojobs > 1. If not
            specified, maximum region size will be set to the size of the smallest region from either `regions` or the
            BAM header.
```
The only option unique to `call` is:
```
    -m, --model <MODEL>                
            Use `MODEL` call model.
```
This sets the model used for calling technical artefacts. If you do not wish to train a model yourself, you can either:

 * Use one of the provided pre-trained models in the `models/` subdirectory. (Make sure the features used in the extract features step are the same as the ones that were used for model training).
 * Use a simple mean separability threshold by specifying it via `--model threshold:X` where `X` is a floating point number such as `0.8`.


### `tune`
```
USAGE:
    spalter tune [FLAGS] [OPTIONS] <FEATURES> <VCF>

FLAGS:
    -h, --help       Prints help information
    -p, --perf       Assess training performance using 10-fold cross validation
    -V, --version    Prints version information

OPTIONS:
    -c, --classifier <CLASSIFIER>    Train a `CLASSIFIER` model. [default: rforest]  [possible values: lsvm, rbfsvm,
                                     dtree, dtree0, dtree1, dtree4, rforest, threshold]
    -e, --exclude <VCF>              Skip sites from `VCF`.
    -n, --num-sites <NUM>            Restrict training to a random subsample of size `NUM`.
    -o, --output <PATH>              Output filename. If omitted, writes to stdout.

ARGS:
    <FEATURES>    Input feature file (.sfts.gz)
    <VCF>         Input SNV/SNP calls (exclude indels)

```

### `guess`
Since error and SNV sites aren't usually known beforehand, training a model using `tune` is not always possible. However, errors and SNVs exhibit different mean separability distributions, especially regarding their modes, as can be seen in the following exemplary histogram:

```
Count
↑                                                                                                                                                                ▒          
|                                                              ▒▒          
|                                                              ▒▒          
|                                                             ▒▒▒▒         
|                                                             ▒▒▒▒         
|                                                             ▒▒▒▒         
|                                                             ▒▒▒▒▒        
|                                                            ▒▒▒▒▒▒        
|                                                            ▒▒▒▒▒▒        
|                                                            ▒▒▒▒▒▒        
|                                                           ▒▒▒▒▒▒▒        
|                                                           ▒▒▒▒▒▒▒        
|                                                          ▒▒▒▒▒▒▒▒▒       
|                                                          ▒▒▒▒▒▒▒▒▒       
|                                                          ▒▒▒▒▒▒▒▒▒       
|                                                         ▒▒▒▒▒▒▒▒▒▒       
|                                                         ▒▒▒▒▒▒▒▒▒▒       
|                                                         ▒▒▒▒▒▒▒▒▒▒       
|                                                         ▒▒▒▒▒▒▒▒▒▒       
|                                                       ▒▒▒▒▒▒▒▒▒▒▒▒▒      
|                                                       ▒▒▒▒▒▒▒▒▒▒▒▒▒      
|                                                       ▒▒▒▒▒▒▒▒▒▒▒▒▒      
|                                                      ▒▒▒▒▒▒▒▒▒▒▒▒▒▒      
|                                                     ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒      
|                          ░                          ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒      
|                       ░░░░░░                       ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒      
|                      ░░░░░░░░                     ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒      
|                    ░░░░░░░░░░░░                  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒     
|                   ░░░░░░░░░░░░░░                ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒     
|                  ░░░░░░░░░░░░░░░░              ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒     
|                  ░░░░░░░░░░░░░░░░░░           ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒     
|                 ░░░░░░░░░░░░░░░░░░░░░       ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒    
|                ░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒    
|               ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒    
|              ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒   
|             ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒   
|           ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒  
|        ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒  
|                                           ><                              
+-//-----------------------------------------------------------------------→ Mean separability in %
     58        64        70        76        81        87        93        99
```
Notice that we can discern two overlapping distributions: The one to the left describes SNVs while the one on the right is due to sequencing errors. Finding a minimum between the modes of these two distributions leads to a reasonable guess for a mean separability threshold.  
Therefore, a very simple threshold estimation procedure is provided via
`spalter guess input.sfts.gz`. Additionally, specifying the flag `-H` will display a histogram similar to the one above. Note that the threshold estimation can also be done at the end of either the `call` or `features` step by providing the `--estimate-threshold` flag.


[^1]: This is likely going to change in future versions; specifying (at least) *two* VCF or BED files is more flexible and makes less assumptions about what is and what is not a SNP/a technical artefact/..

## Authors

* **Till Hartmann**
* **Sven Rahmann**


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

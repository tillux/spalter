use data::{confusion_matrix, ConfusionMatrix};
use rustlearn::array::dense::Array;
use rustlearn::prelude::{IndexableMatrix, RowIterable};
use rustlearn::traits::SupervisedModel;

#[derive(RustcEncodable, RustcDecodable)]
pub struct Threshold {
    threshold: Option<f32>
}

impl Threshold {
    pub fn new() -> Threshold {
        Threshold { threshold: None }
    }

    pub fn from_value(threshold: f32) -> Threshold {
        Threshold { threshold: Some(threshold) }
    }
}

impl<'a> SupervisedModel<&'a Array> for Threshold {
    fn fit(&mut self, x: &'a Array, y: &Array) -> Result<(), &'static str> {
        // simple linear search with a granularity of 0.005 mean sep per step
        let perf = |c: ConfusionMatrix| c.accuracy();
        let mut best_value = 0.;
        let mut best_threshold = 0.;
        for threshold in 0..=200 {
            let t = threshold as f32 / 200.;
            self.threshold = Some(t);
            if let Ok(prediction) = self.predict(x) {
                let confusion = confusion_matrix(&y.data(), &prediction.data());
                let value = perf(confusion);
                if value > best_value {
                    best_value = value;
                    best_threshold = t;
                }
            };
        }
        self.threshold = Some(best_threshold);
        Ok(())
    }

    fn decision_function(&self, x: &Array) -> Result<Array, &'static str> {
        let mut data = Vec::with_capacity(x.rows());
        let threshold = self.threshold.expect("Threshold model has not been trained yet.");
        let cols = x.cols() as f32;
        for row_idx in 0..x.rows() {
            let s: f32 = x.view_row(row_idx).iter().sum();
            let s = s / cols;
            let s = 1. - (s - threshold + 0.5);
            data.push(s);
        }
        Ok(Array::from(data))
    }
}
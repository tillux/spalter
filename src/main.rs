extern crate bincode;
extern crate bio;
#[macro_use]
extern crate clap;
extern crate cue;
extern crate flate2;
extern crate itertools;
#[macro_use]
extern crate log;
extern crate num_cpus;
extern crate pbr;
extern crate rand;
extern crate rust_htslib;
extern crate rustc_serialize;
extern crate rustlearn;
extern crate simplelog;
extern crate spalter;
extern crate term_size;

use clap::{App, ArgMatches};
use flate2::Compression;
use flate2::write::GzEncoder;
use itertools::Itertools;
use rand::SeedableRng;
use rust_htslib::bam;
use rustlearn::ensemble::random_forest::Hyperparameters as RandomForestParams;
use rustlearn::factorization::factorization_machines::Hyperparameters as FactorizationParams;
use rustlearn::linear_models::sgdclassifier::Hyperparameters as SGDParams;
use rustlearn::prelude::*;
use rustlearn::svm::libsvm::svc::Hyperparameters as SVCParams;
use rustlearn::trees::decision_tree::Hyperparameters as DecisionTreeParams;
use simplelog::{CombinedLogger, Config, LevelFilter, TermLogger};
use spalter::cli::{parse_common_args, parse_regions};
use spalter::cli::{SharedWriter, SpaltReader};
use spalter::cli::{write_data, write_vcf_entry};
use spalter::cli::_print_histogram;
use spalter::data::{ColumnIndex, cross_validate2, normalize};
use spalter::data::{Call, Model, SpaltCall, SpaltData, SpaltInfo};
use spalter::data::{build_histogram, guess_threshold, smooth_usize};
use spalter::data::{MetaFeatures, RawFeatures};
use spalter::data::confusion_matrix;
use spalter::lib::{FeaturePileup, FeaturePileupReader};
use spalter::lib::{get_regions, Region, split_region_max};
use spalter::lib::{Label, Position, Separability};
use spalter::lib::{read_features, read_vcf_positions};
use spalter::lib::PileupReaderSettings;
use spalter::threshold::Threshold;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io;
use std::io::Write;
use std::sync::{Arc, Mutex};
use std::sync::atomic::AtomicUsize;

fn spalt(pileup: FeaturePileup, classifiers: &Classifiers) -> (SpaltInfo, SpaltData) {
    let mat = pileup.features;
    let labels = pileup.labels;
    let num_items = pileup._num_items;
    let num_features = pileup._num_features;

    let num_class1 = labels.iter().filter(|&a| *a == 1.).count();
    let num_class2 = labels.iter().filter(|&a| *a != 1.).count();

    let num_feature_combinations: usize = num_features + 1;
    let num_models = classifiers.len();
    let num_entries = num_feature_combinations * num_models;
    let mut accuracy_matrix: Vec<f32> = Vec::with_capacity(num_entries);

    let mut features = Array::from(mat);
    features.reshape(num_items, num_features);
    let features = normalize(&features);
    let targets = Array::from(labels);

    // Make sure random models (such as decision trees and random forests) behave consistently across all feature combinations
    // TODO: make this a parameter; cannot use thread_rng here because that'd lead to different seeds for multiple threads
    let seed: &[_] = &[42, 1337, 141353, 23];
    for &i in &[1, num_features] {
        for col_idx in (0..num_features).combinations(i) {
            let selected_features = features.get_columns(&col_idx);
            let constant = selected_features.data().iter().all_equal();
            let mut models: Vec<Model> = Vec::with_capacity(num_models);
            for c in classifiers.iter() {
                models.push(match &c as &str {
                    "lsvm" => Model::Lsvm(Box::new(SVCParams::new(i, rustlearn::svm::libsvm::svc::KernelType::Linear, 2).build())),
                    "sgd" => Model::SGD(Box::new(SGDParams::new(i).l2_penalty(0.5).build())),
                    "fm" => Model::FM(Box::new(FactorizationParams::new(i, num_features).l2_penalty(0.5).rng(SeedableRng::from_seed(seed)).build())),
                    "rbfsvm" => Model::Rbfsvm(Box::new(SVCParams::new(i, rustlearn::svm::libsvm::svc::KernelType::RBF, 2).build())),
                    "dtree" => Model::DTree(Box::new(DecisionTreeParams::new(i).rng(SeedableRng::from_seed(seed)).max_depth(3).max_features(i).build())),
                    _ => panic!("Unknown classifier {}", c)
                });
            }

            for mut model in &mut models {
                if !constant && model.fit(&selected_features, &targets).is_ok() {
                    if let Ok(prediction) = model.predict(&selected_features) {
                        let accuracy = confusion_matrix(&targets.data(), &prediction.data()).accuracy();
                        accuracy_matrix.push(accuracy);
                    } else {
                        accuracy_matrix.push(0.);
                    }
                } else {
                    accuracy_matrix.push(0.);
                }
            }
        }
    }
    (SpaltInfo {
        tname: pileup.tname,
        tid: pileup.tid,
        ref_pos: pileup.ref_pos,
        ref_base: pileup.ref_base,
        alt_bases: pileup.alt_bases,
        num_class1,
        num_class2,
    },
     SpaltData {
         meta_features: MetaFeatures {
             feature_rows: num_feature_combinations,
             feature_cols: num_models,
             feature_data: accuracy_matrix,
         },
         raw_features: RawFeatures {
             feature_data: features.data().clone(),
         },
     })
}

fn call(info: &SpaltInfo, data: &SpaltData, model: &Model) -> (SpaltInfo, SpaltCall) {
    let mut fd = Vec::with_capacity(data.meta_features.feature_data.len());
    fd.extend(&data.meta_features.feature_data);
    let fd = vec![fd];
    let d = Array::from(&fd);

    let v = model.decision_function(&d).expect(&format!("Failed applying decision_function to:\n{:?}", &d)).get(0, 0);
    let l = model.predict(&d).expect(&format!("Failed predicting class for:\n{:?}", &d)).get(0, 0);

    // rough & relative probability estimates for available models:
    // - decision tree: resorts to leaf probability
    // - random forest: resorts to leaf probability (consensus)
    // - lsvm + rbfsvm: sigmoid(1 / |distance to hyperplane|) (rustlearn sadly does not provide libsvm's probability estimates)
    let v = match model {
        Model::Lsvm(_) => 1. - (1. / v).abs().exp() / (1. + (1. / v).abs().exp()), // distance to hyperplane, in R
        Model::Rbfsvm(_) => 1. - (1. / v).abs().exp() / (1. + (1. / v).abs().exp()), // distance to hyperplane, in R
        Model::DTree(_) => v, // leaf probability, in {0, 1}
        Model::RForest(_) => v, // leaf probability, in [0, 1]
        _ => v
    };

    (info.clone(), SpaltCall {
        call: if l == 1. { Call::SNV } else { Call::Error },
        score: v,
    })
}

fn pipeline_from_features<F>(model: &Model,
                             inpath: &str,
                             num_worker: usize,
                             estimate_thresh: bool,
                             writer_fn: F)
    where F: Sync + Fn(&SpaltInfo, Option<&SpaltData>, Option<&SpaltCall>) {
    let reader = SpaltReader::new(&inpath);
    let mut all_mseps: Vec<f32> = Vec::new();
    cue::pipeline("spalter",
                  num_worker,
                  reader,
                  |(info, data): (SpaltInfo, SpaltData)| {
                      let (i, c) = call(&info, &data, &model);
                      (i, data, c)
                  },
                  |(info, r, c)| {
                      if estimate_thresh {
                          let msep = r.meta_features.feature_data.iter().fold(0., |a: f32, b: &f32| a + b) / r.meta_features.feature_data.len() as f32;
                          all_mseps.push(msep);
                      }
                      writer_fn(&info, Some(&r), Some(&c));
                  });
    if estimate_thresh {
        let (w, h) = if let Some((w, h)) = term_size::dimensions() { (w, h) } else { (80, 24) };
        let threshold = estimate_threshold(&all_mseps, 160, 5);
        print_histogram(&all_mseps, threshold, w, h - 1);
    }
}

fn pipeline_from_bam<F, G>(model: &Model,
                           classifiers: &Classifiers,
                           inpath: &str,
                           refpath: &str,
                           num_worker: usize,
                           num_io_worker: usize,
                           worker_fn: G,
                           writer_fn: F,
                           regions: &Vec<Region>,
                           show_progress: bool,
                           estimate_thresh: bool,
                           settings: &PileupReaderSettings)
    where F: Sync + Fn(&SpaltInfo, Option<&SpaltData>, Option<&SpaltCall>),
          G: Sync + Fn(FeaturePileup, &Model, &Classifiers) -> (SpaltInfo, SpaltData, SpaltCall) {

    // Progressbar related code; since FeaturePileupReader skips non-interesting pileups or ref_skip loci
    // it is difficult to know exactly how many pileups are going to get looked at.
    // Since we also allow multiple io and worker threads, item processing order is arbitrary.
    // -> For each region, let maximum_position = max(old_maximum_position, current_item.position)
    // -> so region specific progress = (maximum_position - region.start) / (region.length)
    // -> global progress is basically the mean of all region specific progress
    let pb = Arc::new(Mutex::new(
        pbr::ProgressBar::on(io::stderr(),
                             regions.iter().map(|r| r.end as u64 - r.start as u64).sum())));
    let progress = HashMap::<Region, f32>::new();
    let mprog = Arc::new(Mutex::new(progress));
    let locker = Arc::new(Mutex::new(()));
    let num_regions = regions.len();
    let mut processed_regions = AtomicUsize::new(0);
    let mut all_mseps: Vec<f32> = Vec::new();

    cue::pipeline("spalter_io",
                  num_io_worker,
                  regions.iter(),
                  |region| {
                      let bamfile = &mut bam::IndexedReader::from_path(inpath).expect("Failed opening bamfile or its index.");
                      let reffile = &mut bio::io::fasta::IndexedReader::from_file(&refpath).expect("Failed opening reference fasta or its index.");
                      let mut mean_seps = Vec::new();
                      let &Region { ref tname, tid, start, end } = region;
                      if bamfile.fetch(tid, start, end).is_ok() {
                          let reader =
                              FeaturePileupReader::new_with_settings(bamfile,
                                                                     reffile,
                                                                     tname.clone(),
                                                                     settings.clone(),
                              );
                          cue::pipeline("spalter",
                                        num_worker,
                                        reader,
                                        |item| { worker_fn(item, &model, &classifiers) },
                                        |(info, r, c)| {
                                            writer_fn(&info, Some(&r), Some(&c));
                                            if estimate_thresh {
                                                let mean_sep = r.meta_features.feature_data.iter().fold(0., |a: f32, b: &f32| a + b) / r.meta_features.feature_data.len() as f32;
                                                mean_seps.push(mean_sep);
                                            }
                                            if show_progress {
                                                let _lock = locker.lock().expect("Failed locking progressbar (1).");
                                                {
                                                    {
                                                        let mut m = mprog.lock().expect("Failed locking progressbar (2).");
                                                        let v = if info.ref_pos < start as usize { 0 } else { (info.ref_pos - start as usize) } as f32 / (end - start) as f32;
                                                        let maxv = m.entry(region.clone()).or_insert(v);
                                                        *maxv = maxv.max(v);
                                                    }

                                                    let m = mprog.lock().unwrap();
                                                    let sum: f32 = m.values().sum();

                                                    let new_max;
                                                    {
                                                        let mpb = pb.lock().unwrap();
                                                        new_max = (sum / num_regions as f32 * mpb.total as f32) as u64;
                                                    }
                                                    pb.lock().expect("Failed locking progressbar (3).").set(new_max);
                                                }
                                            }
                                        });
                      }
                      (region.clone(), mean_seps)
                  },
                  |(_region, mseps)| {
                      if estimate_thresh {
                          all_mseps.extend(&mseps);
                      }
                      if show_progress {
                          let l = processed_regions.get_mut();
                          *l += 1;
                          if *l == num_regions {
                              pb.lock().expect("Failed locking progressbar (4).").finish();
                          }
                      }
                  },
    );
    if estimate_thresh {
        let (w, h) = if let Some((w, h)) = term_size::dimensions() { (w, h) } else { (80, 24) };
        let threshold = estimate_threshold(&all_mseps, 160, 5);
        print_histogram(&all_mseps, threshold, w, h - 1);
    }
}

fn estimate_threshold(values: &[f32], nbins: usize, window_size: usize) -> f32 {
    let hist = build_histogram(&values, nbins);
    let smoothed_hist = smooth_usize(&hist, window_size);
    let max = smoothed_hist.iter().fold(0., |a: f32, b: &f32| a.max(*b));
    let scaled_hist: Vec<f32> = smoothed_hist.iter().map(|x| (x / max * 30.).round()).collect();
    let threshold = guess_threshold(&scaled_hist[..]);
    threshold
}

fn print_histogram(values: &[f32], threshold: f32, width: usize, height: usize) {
    let hist = build_histogram(&values, width);
    let smoothed_hist = smooth_usize(&hist, 5);
    let smoothed_hist: Vec<usize> = smoothed_hist.iter().map(|x| x.round() as usize).collect();
    _print_histogram(&smoothed_hist, (threshold * width as f32).floor() as usize, height);
}

fn call_from_features(matches: &ArgMatches) {
    let args = parse_common_args(&matches);
    let inpath = args.inpath.as_str();
    let outpath = args.outpath.as_str();
//    let num_io_worker = args.num_io_worker;  // Not yet implemented
    let num_worker = args.num_worker;
    let model = args.model;

    let mut out: SharedWriter = Arc::new(Mutex::new({
        match outpath {
            "stdout" => Box::new(io::stdout()),
            s if s.ends_with(".gz") => Box::new(GzEncoder::new(io::BufWriter::new(File::create(outpath).expect("Failed creating file.")), Compression::best())),
            _ => Box::new(io::BufWriter::new(File::create(outpath).expect("Failed creating file."))),
        }
    }));
    let out = &mut out;

    let writer_fn: fn(&SpaltInfo, Option<&SpaltData>, Option<&SpaltCall>, &SharedWriter) = write_vcf_entry;
    out.lock().expect("Failed acquiring write-lock").write_all(
        b"##fileformat=VCFv4.1
##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Read depth\">
##INFO=<ID=AAF,Number=1,Type=Float,Description=\"Alternate allele fraction\">
##INFO=<ID=varType,Number=1,Type=String,Description=\"Variant Type, either of SNV, Error or Unknown\">
##INFO=<ID=MSep,Number=1,Type=Float,Description=\"Mean Separability\">
##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">
#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tSPALTER\n").expect("Failed writing vcf header");
    let writer_func =
        |s: &SpaltInfo, d: Option<&SpaltData>, c: Option<&SpaltCall>|
            { writer_fn(s, d, c, out) };

    let estimate_threshold = matches.is_present("estimate_threshold");
    pipeline_from_features(&model, &inpath, num_worker, estimate_threshold, writer_func);
}

type Classifiers = Vec<String>;

fn call_from_bam<F, G>(matches: &ArgMatches, worker_fn: G, writer_fn: F)
    where F: Sync + Fn(&SpaltInfo, Option<&SpaltData>, Option<&SpaltCall>, &SharedWriter),
          G: Sync + Fn(FeaturePileup, &Model, &Classifiers) -> (SpaltInfo, SpaltData, SpaltCall) {
    let args = parse_common_args(&matches);
    let inpath = args.inpath.as_str();
    let outpath = args.outpath.as_str();
    let num_io_worker = args.num_io_worker;
    let num_worker = args.num_worker;
    let settings = args.settings;
    let model = args.model;
    let classifiers = args.classifiers;

    let mut out: SharedWriter = Arc::new(Mutex::new({
        match outpath {
            "stdout" => Box::new(io::stdout()),
            s if s.ends_with(".gz") => Box::new(GzEncoder::new(io::BufWriter::new(File::create(outpath).expect("Failed creating gz file.")), Compression::best())),
            _ => Box::new(io::BufWriter::new(File::create(outpath).expect("Failed creating file."))),
        }
    }));
    let out = &mut out;

    let writer_func =
        |s: &SpaltInfo, d: Option<&SpaltData>, c: Option<&SpaltCall>|
            { writer_fn(s, d, c, out) };

    debug!("Spawning {} io-job{} with {} worker thread{} *each*",
           &num_io_worker,
           if num_io_worker > 1 { "s" } else { "" },
           &num_worker,
           if num_worker > 1 { "s" } else { "" });

    let refpath = matches.value_of("target").expect("Target reference not found.");
    let bamfile = &mut bam::IndexedReader::from_path(inpath).expect("Failed opening bamfile.");
    use rust_htslib::bam::Read;
    let header = bamfile.header().clone();
    let mut regions = match matches.values_of("regions") {
        Some(regionstrings) => parse_regions(regionstrings, &header),
        None => get_regions(&header)
    };

    let region_size: u32 = if matches.is_present("region_size") {
        matches.value_of("region_size").unwrap().parse().expect("Failed parsing region_size.")
    } else {
        regions.iter().map(|r| r.end - r.start).min().expect("Failed calculating region_size.")
    };
    regions = regions.iter().flat_map(|r| split_region_max(r, region_size)).collect();

    let show_progress = matches.is_present("progress");
    let estimate_threshold = matches.is_present("estimate_threshold");
    pipeline_from_bam(&model, &classifiers, &inpath, &refpath, num_worker, num_io_worker, worker_fn, writer_func, &regions, show_progress, estimate_threshold, &settings);
}

fn main_tune(matches: &ArgMatches) {
    let featurefile = matches.value_of("FEATURES").unwrap();
    let vcffile = matches.value_of("VCF").unwrap();
    let skipfile = matches.value_of("exclude");
    let num_sites: usize = if let Some(num) = matches.value_of("num_sites")
        { num.parse().expect("Unable to parse.") } else { usize::max_value() };

    info!("Reading features...");
    // Read features
    let features: HashMap<Position, (SpaltInfo, SpaltData)> = read_features(&featurefile);
    let positions: HashSet<Position> = features.keys().cloned().collect();

    // Determine labels
    let rnd = std::collections::hash_map::RandomState::new();
    let skip: HashSet<Position> = if let Some(skipfile) = skipfile {
        read_vcf_positions(skipfile, &rnd)
    } else {
        HashSet::new()
    };

    let snps: HashSet<Position> = read_vcf_positions(vcffile, &rnd);
    let snp_or_skip: HashSet<Position> = snps.union(&skip).cloned().collect();
    let errs: HashSet<Position> = positions.difference(&snp_or_skip).cloned().collect();
    let snps: HashSet<Position> = snps.difference(&skip).cloned().collect();

    let mut data: Vec<(Separability, Label)> = Vec::with_capacity(positions.len());
    for (pos, (_sinfo, sdata)) in features.into_iter() {
        let label: Option<f32> = if errs.contains(&pos) { Some(0.) } else if snps.contains(&pos) { Some(1.) } else { None };
        if let Some(l) = label {
            data.push((sdata.meta_features.feature_data, l));
        }
    }
    let num_sites = num_sites.min(data.len());
    info!("Read features.");

    let dim = data[0].0.len();
    let mut classifier: Model = match matches.value_of("classifier") {
        Some("threshold") => Model::MeanThreshold(Box::new(Threshold::new())),
        Some("lsvm") => Model::Lsvm(Box::new(SVCParams::new(dim, rustlearn::svm::libsvm::svc::KernelType::Linear, 2).build())),
        Some("rbfsvm") => Model::Rbfsvm(Box::new(SVCParams::new(dim, rustlearn::svm::libsvm::svc::KernelType::RBF, 2).build())),
        Some("dtree") => Model::DTree(Box::new(DecisionTreeParams::new(dim).max_features(dim).build())),
        Some("dtree4") => Model::DTree(Box::new(DecisionTreeParams::new(dim).max_features(dim).max_depth(4).build())),
        Some("dtree1") => Model::DTree(Box::new(DecisionTreeParams::new(dim).max_features(dim).max_depth(1).build())),
        Some("dtree0") => Model::DTree(Box::new(DecisionTreeParams::new(dim).max_features(dim).max_depth(0).build())),
        Some("rforest") => Model::RForest(Box::new(RandomForestParams::new(DecisionTreeParams::new(dim).max_depth(4).max_features(dim).clone(), 19).build())),
        _ => Model::RForest(Box::new(RandomForestParams::new(DecisionTreeParams::new(dim).max_depth(4).max_features(dim).clone(), 19).build())),
    };

    use rand::Rng;
    let mut rng = rand::thread_rng();
    rng.shuffle(&mut data);
    let validation = if num_sites < data.len() { Some(&data[num_sites..]) } else { None };
    let data_subsampled = &data[0..num_sites];
    let training = &data_subsampled[..];

    let outpath = matches.value_of("output").unwrap_or("stdout");
    let mut out: Box<Write> = match outpath {
        "stdout" => Box::new(io::stdout()),
        s if s.ends_with(".gz") => Box::new(GzEncoder::new(io::BufWriter::new(File::create(outpath).expect("Failed creating gz file.")), Compression::best())),
        _ => Box::new(io::BufWriter::new(File::create(outpath).expect("Failed creating file."))),
    };

    if matches.is_present("perf") {
        info!("{}: 10-fold cross validation on {} sites.", outpath, training.len());
        let (features, labels): (Vec<Vec<f32>>, Vec<f32>) = training.iter().cloned().unzip();
        let features = Array::from(&features);
        let labels = Array::from(labels);
        let (accuracy, variance) = cross_validate2(&mut classifier, &features, &labels, 10);
        if writeln!(&mut out, "#{}\naccuracy\tvariance\n{}\t{}", outpath, accuracy, variance).is_err() || out.flush().is_err() {
            error!("Failed writing stats to {}", outpath)
        }
    } else {
        info!("Training model on a subsample of size {}", num_sites);
        let (features, labels): (Vec<Vec<f32>>, Vec<f32>) = training.iter().cloned().unzip();
        let features = Array::from(&features);
        let labels = Array::from(labels);
        if classifier.fit(&features, &labels).is_ok() {
            let prediction = classifier.predict(&features).expect("Prediction failed.");
            let cmat = confusion_matrix(&labels.data(), &prediction.data());
            let num_class1 = labels.data().iter().filter(|&a| *a == 1.).count() as f32;
            let num_class2 = labels.data().iter().filter(|&a| *a != 1.).count() as f32;
            let prior = num_class1.max(num_class2) / (num_class1 + num_class2);
            info!("Model\tType\tAccuracy\tRelativeAccuracy\tMCC");
            info!("{}\tTraining\t{}\t{}\t{}", outpath, cmat.accuracy(), (cmat.accuracy() - prior) / (1. - prior), cmat.mcc());
            if let Some(vdata) = validation {
                let (features, labels): (Vec<Vec<f32>>, Vec<f32>) = vdata.iter().cloned().unzip();
                let features = Array::from(&features);
                let labels = Array::from(labels);
                let prediction = classifier.predict(&features).expect("Prediction failed.");
                let cmat = confusion_matrix(&labels.data(), &prediction.data());
                let num_class1 = labels.data().iter().filter(|&a| *a == 1.).count() as f32;
                let num_class2 = labels.data().iter().filter(|&a| *a != 1.).count() as f32;
                let prior = num_class1.max(num_class2) / (num_class1 + num_class2);
                info!("Model\tType\tAccuracy\tRelativeAccuracy\tMCC");
                info!("{}\tValidation\t{}\t{}\t{}", outpath, cmat.accuracy(), (cmat.accuracy() - prior) / (1. - prior), cmat.mcc());
            }
            info!("Writing model to file {}", outpath);
            let c = classifier;
            let model_json = rustc_serialize::json::encode(&c).expect("Failed to serialize model.");
            if writeln!(&mut out, "{}", model_json).is_err() || out.flush().is_err() {
                error!("Failed writing model to {}", outpath)
            }
        } else {
            error!("Failed training model.");
        }
    }
}

fn main_guess(matches: &ArgMatches) {
    let featurefile = matches.value_of("FEATURES").unwrap();
    let print_hist = matches.is_present("print_histogram");
    let features: HashMap<Position, (SpaltInfo, SpaltData)> = read_features(&featurefile);
    let mut all_mseps = Vec::with_capacity(features.len());
    for (_sinfo, sdata) in features.values() {
        let profile = &sdata.meta_features.feature_data;
        let msep = profile.iter().fold(0., |a: f32, b: &f32| a + b) / profile.len() as f32;
        all_mseps.push(msep);
    }
    let mut thresholds = vec![];
    for n in vec![25, 50, 75, 100] {
        for w in vec![1, 3, 5] {
            if w < n {
                let threshold = estimate_threshold(&all_mseps, n, w);
                if threshold > 0. {
                    thresholds.push(threshold);
                }
            }
        }
    }
    let threshold = thresholds.iter().fold(0., |a: f32, b: &f32| a + b) / thresholds.len() as f32;
    if print_hist {
        let (width, height) = if let Some((tw, th)) = term_size::dimensions() { (tw, th) } else { (80, 24) };
        print_histogram(&all_mseps, threshold, width, height - 1);
    }
    println!("Estimated mean separability threshold: {}", threshold);
}

fn main() {
    CombinedLogger::init(
        vec![
            TermLogger::new(LevelFilter::Info, Config::default()).expect("Failed creating terminal logger."),
//            WriteLogger::new(LevelFilter::Debug, Config::default(), File::create("spalter.log").expect("Failed creating file logger.")),
        ]
    ).expect("Failed initializing loggers.");

    let yaml = load_yaml!("cli.yml");
    let app = App::from_yaml(yaml);

    // This seems to be unnecessarily complex.
    let mut c = std::io::Cursor::new(Vec::new());
    app.write_help(&mut c).unwrap();
    use std::io::{Read, Seek};
    c.seek(std::io::SeekFrom::Start(0)).unwrap();
    let mut help = Vec::new();
    c.read_to_end(&mut help).unwrap();

    let matches = app.get_matches();
    match matches.subcommand() {
        ("call", Some(submatches))
        if submatches.value_of("INPUT").unwrap().ends_with(".sfts")
            || submatches.value_of("INPUT").unwrap().ends_with(".sfts.gz") => call_from_features(submatches), // read features, then call
        ("call", Some(submatches)) => call_from_bam(submatches,
                                                    |item: FeaturePileup, model: &Model, classifiers: &Classifiers| { // extract features, then call
                                                        let r = spalt(item, &classifiers);
                                                        let c = call(&r.0, &r.1, &model);
                                                        (c.0, r.1, c.1)
                                                    },
                                                    write_vcf_entry),
        ("features", Some(submatches)) => call_from_bam(submatches,
                                                        |item: FeaturePileup, _, classifiers: &Classifiers| { // don't call, only extract features
                                                            let r = spalt(item, &classifiers);
                                                            (r.0, r.1, SpaltCall { call: Call::Unknown, score: -1.0 })
                                                        },
                                                        write_data),
        ("tune", Some(submatches)) => main_tune(submatches),
        ("guess", Some(submatches)) => main_guess(submatches),
        x => {
            eprintln!("No such command {:?}.", x.0);
            println!("{}", std::str::from_utf8(&help).unwrap());
        }
    };
}
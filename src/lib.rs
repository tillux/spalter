extern crate bincode;
extern crate bio;
extern crate itertools;
#[macro_use]
extern crate log;
extern crate rust_htslib;
extern crate rustc_serialize;
#[macro_use]
extern crate serde_derive;
extern crate rustlearn;

pub mod lib {
    use std::collections::HashSet;
    use rust_htslib;
    use rust_htslib::bam;
    use std::fs::File;
    use std::fmt;
    use rust_htslib::bam::HeaderView;
    use std::collections::HashMap;
    use bio;
    use lib::features::is_read1;
    use lib::features::is_reverse;
    use lib::features::neighbour_base_quality;
    use cli::SpaltReader;
    use data::{SpaltInfo, SpaltData};


    // type for feature functions that extract/derive a feature given (ref_pos, ref_base, query_pos, read)
    pub type FeatureFn = fn(usize, u8, usize, &bam::Record) -> f32;

    pub mod features {
        use std::f32::consts::PI;

        //const PAIRED: u16 = 0x01;
        const REVERSE: u16 = 0x10;
        const READ1: u16 = 0x40;


        #[allow(unused_variables)]
        pub fn end_proximity(ref_pos: usize, ref_base: u8, query_pos: usize, read: &super::bam::Record) -> f32 {
            let r: f32 = read.seq().len() as f32;
            let x: f32 = (read.pos() as f32) - (r / 2.0);
            ((PI * x) / r).sin().powi(2)
        }

        #[allow(unused_variables)]
        pub fn end_proximity_asymmetric(ref_pos: usize, ref_base: u8, query_pos: usize, read: &super::bam::Record) -> f32 {
            let r: f32 = read.seq().len() as f32;
            let x: f32 = (read.pos() as f32) - (r / 2.0);
            (x / (PI * r.powf(1. / 3.))).sin().powi(3)
        }

        #[allow(unused_variables)]
        pub fn neighbour_base_quality(ref_pos: usize, ref_base: u8, query_pos: usize, read: &super::bam::Record) -> f32 {
            let rpos = ref_pos - read.pos() as usize;
            let qual = read.qual();
            let qlen = qual.len();
            let mut result: f32 = 0.;
            if 1 < rpos && rpos < qlen - 1 {
                let s: u64 = qual[rpos - 1..rpos + 2].iter().fold(0, |acc, &x| acc + x as u64);
                result = s as f32 / 3.;
            } else if rpos == 0 {
                result = (qual[rpos] + qual[rpos + 1]) as f32 / 2.;
            } else if rpos == qlen - 1 {
                result = (qual[rpos - 1] + qual[rpos]) as f32 / 2.;
            }
            // // PHRED starts at 33 = '!', ends at 127 = '~'
            // // scaling to [-1, 1] has an impact on performance
            result = (result - 33.) / ((127. - 33.) / 2.) - 1.;
            result
        }

        #[allow(unused_variables)]
        pub fn is_reverse(ref_pos: usize, ref_base: u8, query_pos: usize, read: &super::bam::Record) -> f32 {
            let flags = read.flags();
            if flags & REVERSE > 0 { 1. } else { -1. }
        }

        #[allow(unused_variables)]
        pub fn is_read1(ref_pos: usize, ref_base: u8, query_pos: usize, read: &super::bam::Record) -> f32 {
            let flags = read.flags();
            if flags & READ1 > 0 { 1. } else { -1. }
        }
    }


    #[derive(Clone)]
    pub struct PileupReaderSettings {
        pub min_alt_frac: f32,
        pub min_read_depth: usize,
        pub feature_functions: Vec<FeatureFn>,
        pub skip: HashSet<(u32, u32)>,
    }

    impl PileupReaderSettings {
        pub fn new(min_alt_frac: f32, min_read_depth: usize, feature_functions: Vec<FeatureFn>, skip: HashSet<(u32, u32)>) -> PileupReaderSettings {
            PileupReaderSettings {
                min_alt_frac,
                min_read_depth,
                feature_functions,
                skip,
            }
        }
    }

    impl Default for PileupReaderSettings {
        fn default() -> Self {
            PileupReaderSettings {
                min_alt_frac: 0.2,
                min_read_depth: 20,
                feature_functions: vec![is_read1, is_reverse, neighbour_base_quality],
                skip: HashSet::new(),
//                feature_functions: vec![is_read1, end_proximity, is_reverse, is_mirror_reverse, neighbour_base_quality],
            }
        }
    }

    pub struct FeaturePileup {
        pub tname: Vec<u8>,
        pub tid: u32,
        pub ref_pos: usize,
        pub ref_base: u8,
        pub _num_items: usize,
        pub _num_features: usize,
        pub alt_bases: Vec<char>,
        pub features: Vec<f32>,
        pub labels: Vec<f32>,
    }

    pub struct FeaturePileupReader<'a, T: 'a + bam::Read> {
        counts: [u32; 32],
        pileups: bam::pileup::Pileups<'a, T>,
        reference: &'a mut bio::io::fasta::IndexedReader<File>,
        tname: String,
        settings: PileupReaderSettings,
    }

    impl<'a, T: 'a + bam::Read> FeaturePileupReader<'a, T> {
        pub fn new(bamfile: &'a mut T,
                   reference: &'a mut bio::io::fasta::IndexedReader<File>,
                   tname: Vec<u8>) -> FeaturePileupReader<'a, T> {
            let counts: [u32; 32] = [0; 32];
            FeaturePileupReader {
                counts,
                pileups: bamfile.pileup(),
                reference,
                tname: String::from_utf8(tname).unwrap(),
                settings: PileupReaderSettings::default(),
            }
        }

        pub fn new_with_settings(bamfile: &'a mut T,
                                 reference: &'a mut bio::io::fasta::IndexedReader<File>,
                                 tname: Vec<u8>,
                                 settings: PileupReaderSettings) -> FeaturePileupReader<'a, T> {
            let counts: [u32; 32] = [0; 32];
//        let feature_functions: Vec<FeatureFn> = vec![is_mirror_reverse, is_read1, end_proximity, neighbour_base_quality];
            FeaturePileupReader {
                counts,
                pileups: bamfile.pileup(),
                reference,
                tname: String::from_utf8(tname).unwrap(),
                settings,
            }
        }
    }

    #[inline]
    fn count_into(sink: &mut [u32], seq: &[u8]) {
        for c in seq {
            sink[*c as usize % 32] += 1; // for ascii: [a-z] ==_{mod 32} [A-Z] (-> case insensitivity)
        }
    }

    fn is_interesting(counts: &[u32], threshold: f32) -> bool {
        let num_items: u32 = counts.iter().sum();
        let num_items: f32 = num_items as f32;
        let mut i: u8 = 0;
        for c in counts {
            let fraction: f32 = (*c as f32) / num_items;
            if fraction >= threshold {
                i += 1;
                if i >= 2 {
                    return true;
                }
            }
        }
        false
    }

    impl<'a, T: 'a + bam::Read> Iterator for FeaturePileupReader<'a, T> {
        type Item = FeaturePileup;

        fn next(&mut self) -> Option<Self::Item> {
            while let Some(pileup) = self.pileups.next() {
                let pileup = pileup.expect("Error reading pileup");
                if self.settings.skip.contains(&(pileup.tid(), pileup.pos())) {
                    continue;
                }
                let depth = pileup.depth() as usize;

                if depth < self.settings.min_read_depth {
                    continue;
                }

                let ref_pos = pileup.pos() as usize;
                let tid = pileup.tid();
                let num_features = self.settings.feature_functions.len();
                let mut pileupbases = Vec::with_capacity(depth);
                let mut features = Vec::<f32>::with_capacity(depth * num_features);


                // get reference base at the current position
                let mut ref_base = vec![0];
                let seqname = &self.tname;
                self.reference.fetch(seqname, ref_pos as u64, (ref_pos + 1) as u64).expect("Failed fetching reference base");
                let r = self.reference.read(&mut ref_base);
                let ref_base: u8 = match r {
                    Ok(_) => (ref_base[0] % 32) + 64,
                    _ => continue,
                };

                let ffs = &self.settings.feature_functions;
                for alignment in pileup.alignments() {
                    if !alignment.is_del() && !alignment.is_refskip() {
                        let record = &alignment.record();
                        let query_pos = alignment.qpos().unwrap(); // will never be None, since we check for is_del and is_refskip
                        pileupbases.push(alignment.record().seq()[query_pos]);
                        for ff in ffs {
                            features.push(ff(ref_pos, ref_base, query_pos, record));
                        }
                    }
                }
                for i in 0..32 {
                    self.counts[i] = 0;
                }
                use itertools::Itertools;
                count_into(&mut self.counts, &pileupbases);
                let num_items = pileupbases.len();
                if num_items >= self.settings.min_read_depth && is_interesting(&self.counts, self.settings.min_alt_frac) {
                    let labels: Vec<f32> = pileupbases.iter().map(|x| ((x % 32) == (ref_base % 32)) as u16 as f32).collect();
                    let alt_bases: Vec<char> = self.counts.iter()
                        .enumerate()
                        .filter(|&(i, &x)| x > 0u32 && i as u8 != ref_base % 32)
                        .map(|(i, _)| (i + 64) as u8 as char).collect();
                    if !alt_bases.is_empty() && !labels.iter().all_equal() {
                        let tname: Vec<u8> = self.tname.as_bytes().to_vec();
                        return Some(FeaturePileup { tname, tid, ref_pos, ref_base, features, labels, alt_bases, _num_items: num_items, _num_features: num_features });
                    }
                }
            }
            None
        }
    }

    #[derive(PartialEq, Eq, Hash, Debug, Clone)]
    pub struct Region {
        pub tname: Vec<u8>,
        pub tid: u32,
        pub start: u32,
        pub end: u32,
    }

    impl fmt::Display for Region {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "{}: {}-{}", String::from_utf8_lossy(&self.tname), self.start, self.end)
        }
    }

    pub fn get_regions(header: &HeaderView) -> Vec<Region> {
        let target_names = header.target_names();
        let target_tids: Vec<u32> = target_names.iter().map(|&tname| header.tid(tname).unwrap()).filter(|&tid| tid < 22).collect();
        let target_lengths: Vec<u32> = target_tids.iter().map(|&tid| header.target_len(tid).unwrap()).collect();
        target_names.iter().zip(target_tids.iter().zip(target_lengths.iter()))
            .map(|(&tname, (&tid, &end))| Region { tname: tname.iter().map(|&u| u).collect(), tid, start: 0, end }).collect()
    }

    pub fn splitn_region(region: &Region, n: u32) -> Vec<Region> {
        (0..n).map(|i| Region {
            tname: region.tname.clone(),
            tid: region.tid,
            start: region.start + i * (region.end - region.start) / n,
            end: if i < n - 1 { region.start + (i + 1) * (region.end - region.start) / n } else { region.end },
        }).collect()
    }

    pub fn split_region_max(region: &Region, max_length: u32) -> Vec<Region> {
        let n = (region.end - region.start) / max_length;
        (0..n).map(|i| Region {
            tname: region.tname.clone(),
            tid: region.tid,
            start: region.start + i * max_length,
            end: if i < n - 1 { region.start + (i + 1) * max_length } else { region.end },
        }).collect()
    }

    pub fn split_regions(regions: Vec<Region>, n: u32) -> Vec<Region> {
        if n == 0 {
            return regions;
        }
        let lengths: Vec<u32> = regions.iter().map(|r| r.end - r.start).collect();
        use itertools::Itertools;
        let mut xs: Vec<Region> = regions.into_iter()
            .zip(lengths.into_iter())
            .sorted_by(|&(_, l1), &(_, l2)| l1.cmp(&l2)) // ascending
            .into_iter()
            .map(|(r, _)| r)
            .collect();

        if let Some(r) = xs.pop() { // largest region is the last one
            let mut sub_regions = splitn_region(&r, 2);
            xs.append(&mut sub_regions);
            return split_regions(xs, n - 1);
        } else {
            return xs;
        }
    }

    pub fn split2<'a>(s: &'a str, pat: char) -> (&'a str, &'a str) {
        let mut splitter = s.splitn(2, pat);
        let a = splitter.next().unwrap();
        let b = splitter.next().unwrap();
        (a, b)
    }

    pub fn read_vcf_positions(path: &str, hasher: &super::std::collections::hash_map::RandomState) -> HashSet<Position> {
        let mut vcf_reader = rust_htslib::bcf::Reader::from_path(path).expect("Failed opening vcf file.");
        let mut vcf_positions = HashSet::with_hasher(hasher.clone());
        use rust_htslib::bcf::Read;
        for mut record in vcf_reader.records() {
            if let Ok(mut record) = record {
                if let Some(rid) = record.rid() {
                    let pos = record.pos();
                    vcf_positions.insert((rid, pos));
                }
            }
        }
        vcf_positions
    }

    // (separability profile, label)
    pub type Separability = Vec<f32>;
    pub type Label = f32;
    // (tid, pos)
    pub type Position = (u32, u32);


    pub fn read_features(path: &str) -> HashMap<Position, (SpaltInfo, SpaltData)> {
        let spaltr_reader = SpaltReader::new(path);
        let mut data: HashMap<Position, (SpaltInfo, SpaltData)> = HashMap::new();
        for (sinfo, sdata) in spaltr_reader {
            let tid = sinfo.tid as u32;
            let pos = sinfo.ref_pos as u32;
            data.insert((tid, pos), (sinfo, sdata));
        }
        data
    }
}

pub mod threshold;

pub mod data;

pub mod cli;
extern crate itertools;
extern crate rustc_serialize;
extern crate rustlearn;
extern crate std;

use self::rustlearn::array::dense::Array;
use self::rustlearn::array::dense::ArrayView;
use self::rustlearn::ensemble::random_forest;
use self::rustlearn::factorization::factorization_machines;
use self::rustlearn::linear_models::sgdclassifier;
use self::rustlearn::prelude::*;
use self::rustlearn::svm::libsvm::svc;
use self::rustlearn::trees::decision_tree;
use std::collections::HashMap;
use threshold::Threshold;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SpaltInfo {
    pub tname: Vec<u8>,
    pub tid: u32,
    pub ref_pos: usize,
    pub ref_base: u8,
    pub alt_bases: Vec<char>,
    pub num_class1: usize,
    pub num_class2: usize,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SpaltData {
    pub meta_features: MetaFeatures,
    pub raw_features: RawFeatures,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RawFeatures {
    pub feature_data: Vec<f32>
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MetaFeatures {
    pub feature_rows: usize,
    pub feature_cols: usize,
    pub feature_data: Vec<f32>,
}

pub enum Call {
    SNV,
    Error,
    Unknown,
}

impl std::fmt::Display for Call {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", match self {
            &Call::SNV => "SNV",
            &Call::Error => "ERR",
            &Call::Unknown => "UK",
        })
    }
}

pub struct SpaltCall {
    pub call: Call,
    pub score: f32,
}

impl std::fmt::Display for SpaltInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:>2}, {:>11}: {:>3}:{:<3}",
               self.tid,
               self.ref_pos,
               self.num_class1,
               self.num_class2,
        )
    }
}

impl std::fmt::Display for SpaltData {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} x {}", &self.meta_features.feature_rows, &self.meta_features.feature_cols)
    }
}

pub trait MinMaxMean {
    fn minmaxmean(&mut self) -> (f32, f32, f32);
}

impl<T: Iterator<Item=f32>> MinMaxMean for T {
    fn minmaxmean(&mut self) -> (f32, f32, f32) {
        let mut min = std::f32::INFINITY;
        let mut max = -std::f32::INFINITY;
        let mut sum = 0_f32;
        let mut n = 0_u32;
        for x in self {
            if x > max {
                max = x;
            }
            if x < min {
                min = x;
            }
            sum += x;
            n += 1;
        }
        (min, max, sum / n as f32)
    }
}


pub fn minmaxmean(d: &[f32]) -> (f32, f32, f32) {
    let mut min = std::f32::INFINITY;
    let mut max = -std::f32::INFINITY;
    let mut sum = 0_f32;
    let mut n = 0_u32;
    for &x in d {
        if x > max {
            max = x;
        }
        if x < min {
            min = x;
        }
        sum += x;
        n += 1;
    }
    (min, max, sum / n as f32)
}

// Incremental update of mean and variance
// 1.: `let mut agg = (0., 0., 0.)`
// 2.: `update_mean_and_var(&mut agg, value)`
// 3.: `let (mean, var) = mean_and_var_from(&agg)`.
pub type MeanVar = (f32, f32);

#[inline]
pub fn update_mean_and_var(agg: &mut (f32, f32, f32), x: f32) {
    let (ref mut count, ref mut mean, ref mut squared_dist_from_mean) = *agg;
    *count += 1.;
    let delta = x - *mean;
    *mean += delta / *count;
    let delta2 = x - *mean;
    *squared_dist_from_mean += delta * delta2;
}

#[inline]
pub fn mean_and_var_from(agg: &(f32, f32, f32)) -> MeanVar {
    let (count, mean, squared_dist_from_mean) = *agg;
    (mean, squared_dist_from_mean / (count - 1.))
}

pub fn normalize(array: &Array) -> Array {
    let (rows, cols) = (array.rows(), array.cols());
    let mut data: Vec<f32> = vec![0.; rows * cols];

    for (i, col) in array.iter_columns().enumerate() {
        let col: ArrayView = col;
        let sum: f32 = col.iter().sum();
        let mean = sum / rows as f32;
        let sdev: f32 = col.iter().map(|x| (x - mean).powi(2)).sum();
        let std: f32 = (sdev / (rows as f32)).sqrt();  // should actually be `rows - 1`
        let centered: Vec<f32> = col.iter().map(|x| (x - mean) / std).collect();
        for (r, &v) in centered.iter().enumerate() {
            data[i + r * cols] = v;
        }
    }
    let mut normalized = Array::from(data);
    normalized.reshape(rows, cols);
    normalized
}

#[derive(Debug, Clone)]
pub struct ConfusionMatrix {
    true_positives: u32,
    false_positives: u32,
    false_negatives: u32,
    true_negatives: u32,
}

impl ConfusionMatrix {
    pub fn new() -> ConfusionMatrix {
        ConfusionMatrix {
            true_positives: 0,
            false_positives: 0,
            false_negatives: 0,
            true_negatives: 0,
        }
    }
    pub fn ppv(&self) -> f32 {
        self.true_positives as f32 / (self.true_positives as f32 + self.false_positives as f32)
    }

    pub fn npv(&self) -> f32 {
        self.true_negatives as f32 / (self.true_negatives as f32 + self.false_negatives as f32)
    }

    pub fn tpr(&self) -> f32 {
        self.true_positives as f32 / (self.true_positives as f32 + self.false_negatives as f32)
    }

    pub fn tnr(&self) -> f32 {
        self.true_negatives as f32 / (self.true_negatives as f32 + self.false_negatives as f32)
    }

    pub fn fnr(&self) -> f32 {
        1. - self.tpr()
    }

    pub fn fpr(&self) -> f32 {
        1. - self.tnr()
    }

    pub fn fdr(&self) -> f32 {
        1. - self.ppv()
    }

    pub fn accuracy(&self) -> f32 {
        (self.true_positives + self.true_negatives) as f32 / (self.true_positives + self.true_negatives + self.false_positives + self.false_negatives) as f32
    }

    pub fn f1score(&self) -> f32 {
        (2 * self.true_positives) as f32 / (2 * self.true_positives + self.false_positives + self.false_negatives) as f32
    }

    pub fn mcc(&self) -> f32 {
        let tp = self.true_positives as f32;
        let tn = self.true_negatives as f32;
        let fp = self.false_positives as f32;
        let vn = self.false_negatives as f32;
        (tp * tn - fp * vn) / ((tp + fp) * (tp + vn) * (tn + fp) * (tn + vn)).sqrt()
    }

    pub fn metrics(&self) -> HashMap<&str, f32> {
        let mut metricsmap = HashMap::new();
        metricsmap.insert("true positive rate / sensitivity / recall", self.tpr());
        metricsmap.insert("true negative rate / specificity", self.tnr());
        metricsmap.insert("false negative rate / miss rate", self.fnr());
        metricsmap.insert("false positive rate / fall-out", self.fpr());
        metricsmap.insert("positive predictive value / precision", self.ppv());
        metricsmap.insert("negative predictive value", self.npv());
        metricsmap.insert("false discovery rate", self.fdr());
        metricsmap.insert("accuracy", self.accuracy());
        metricsmap.insert("F1 score", self.f1score());
        metricsmap.insert("Matthews correlation coefficient", self.mcc());
        metricsmap
    }
}

pub fn confusion_matrix(y_true: &[f32], y_pred: &[f32]) -> ConfusionMatrix {
    let mut matrix = ConfusionMatrix::new();
    let pairs = y_true.iter().zip(y_pred.iter());
    for (t, p) in pairs {
        if *t == 1. {
            if t == p {
                matrix.true_positives += 1;
            } else {
                matrix.false_positives += 1;
            }
        } else {
            if t == p {
                matrix.true_negatives += 1;
            } else {
                matrix.false_negatives += 1;
            }
        }
    }
    matrix
}

// pub type Model = Box<for<'a> SupervisedModel<&'a Array>>;
#[derive(RustcEncodable, RustcDecodable)]
pub enum Model {
    Lsvm(Box<svc::SVC>),
    Rbfsvm(Box<svc::SVC>),
    DTree(Box<decision_tree::DecisionTree>),
    RForest(Box<random_forest::RandomForest>),
    SGD(Box<sgdclassifier::SGDClassifier>),
    FM(Box<factorization_machines::FactorizationMachine>),
    MeanThreshold(Box<Threshold>),
}

impl<'a> SupervisedModel<&'a Array> for Model {
    fn fit(&mut self, x: &'a Array, y: &Array) -> Result<(), &'static str> {
        match self {
            Model::Lsvm(m) => m.fit(x, y),
            Model::Rbfsvm(m) => m.fit(x, y),
            Model::DTree(m) => m.fit(x, y),
            Model::RForest(m) => m.fit(x, y),
            Model::SGD(m) => m.fit(x, y),
            Model::FM(m) => m.fit(x, y),
            Model::MeanThreshold(m) => m.fit(x, y),
        }
    }

    fn decision_function(&self, x: &Array) -> Result<Array, &'static str> {
        match self {
            Model::Lsvm(m) => m.decision_function(x),
            Model::Rbfsvm(m) => m.decision_function(x),
            Model::DTree(m) => m.decision_function(x),
            Model::RForest(m) => m.decision_function(x),
            Model::SGD(m) => m.decision_function(x),
            Model::FM(m) => m.decision_function(x),
            Model::MeanThreshold(m) => m.decision_function(x),
        }
    }
}

//impl<'a> ParallelSupervisedModel<&'a Array> for Model {
//    fn fit_parallel(&mut self, X: &Array, y: &Array, num_threads: usize) -> Result<(), &'static str> {
//        match self {
//            Model::Lsvm(m) => m.fit(X, y),
//            Model::Rbfsvm(m) => m.fit(X, y),
//            Model::DTree(m) => m.fit_parallel(X, y, num_threads),
//            Model::RForest(m) => m.fit_parallel(X, y, num_threads),
//        }
//    }
//}

//impl<'a> ParallelPredict<&'a Array> for Model {
//    fn predict_parallel(&self, X: &Array, num_threads: usize) -> Result<Array, &'static str> {
//        match self {
//            Model::Lsvm(m) => m.predict(X),
//            Model::Rbfsvm(m) => m.predict(X),
//            Model::DTree(m) => m.predict_parallel(X, num_threads),
//            Model::RForest(m) => m.predict_parallel(X, num_threads),
//        }
//    }
//    fn decision_function_parallel(&self, X: &Array, num_threads: usize) -> Result<Array, &'static str> {
//        match self {
//            Model::Lsvm(m) => m.decision_function(X),
//            Model::Rbfsvm(m) => m.decision_function(X),
//            Model::DTree(m) => m.decision_function_parallel(X, num_threads),
//            Model::RForest(m) => m.decision_function_parallel(X, num_threads),
//        }
//    }
//}

pub fn cross_validate2(model: &mut Model, features: &Array, targets: &Array, n_folds: usize) -> MeanVar {
    let num_items = features.rows();
    let mut var_agg = (0., 0., 0.);
    let cv = rustlearn::cross_validation::CrossValidation::new(num_items, n_folds);
    for (train_idx, validate_idx) in cv {
        let x_train: Array = features.get_rows(&train_idx);
        let y_train: Array = targets.get_rows(&train_idx);
        let x_validate: Array = features.get_rows(&validate_idx);
        let y_validate: Array = targets.get_rows(&validate_idx);

        if model.fit(&x_train, &y_train).is_ok() {
            if let Ok(prediction) = model.predict(&x_validate) {
                let confusion = confusion_matrix(y_validate.data(), prediction.data());
                let accuracy = confusion.accuracy();
                update_mean_and_var(&mut var_agg, accuracy);
            }
        }
    }
    mean_and_var_from(&var_agg)
}

pub fn cross_validate(model: &mut Model, features: &Array, targets: &Array, n_folds: usize) -> MeanVar {
    let num_items = features.rows();
    let mut var_agg = (0., 0., 0.);
    if n_folds == num_items || n_folds <= 1 {
        if model.fit(features, targets).is_ok() {
            if let Ok(prediction) = model.predict(features) {
                let confusion = confusion_matrix(&targets.data(), &prediction.data());
//                let mut prior = rustlearn::metrics::accuracy_score(targets, &Array::from(vec![1.; num_items]));
//                prior = prior.max(1. - prior);
//                let mut accuracy = rustlearn::metrics::accuracy_score(targets, &prediction);
//                accuracy = accuracy.max(1. - accuracy);
//                return (get_relative_accuracy(accuracy, prior), 0.);
                return (confusion.mcc(), 0.);
            }
        }
    } else {
        let cv = rustlearn::cross_validation::CrossValidation::new(num_items, n_folds);
        for (train_idx, validate_idx) in cv {
            let x_train: Array = features.get_rows(&train_idx);
            let y_train: Array = targets.get_rows(&train_idx);
            let x_validate: Array = features.get_rows(&validate_idx);
            let y_validate: Array = targets.get_rows(&validate_idx);
            let mut prior = rustlearn::metrics::accuracy_score(&y_validate, &Array::from(vec![1.; y_validate.rows()]));
            prior = prior.max(1. - prior);

            if model.fit(&x_train, &y_train).is_ok() {
                if let Ok(prediction) = model.predict(&x_validate) {
                    let mut accuracy = rustlearn::metrics::accuracy_score(&y_validate, &prediction);
                    accuracy = accuracy.max(1. - accuracy);

                    update_mean_and_var(&mut var_agg, get_relative_accuracy(accuracy, prior));
                }
            }
        }
        return mean_and_var_from(&var_agg);
    }
    (0., 0.)
}

#[inline]
fn get_relative_accuracy(accuracy: f32, prior: f32) -> f32 {
    if accuracy >= prior && prior < 1. { (accuracy - prior) / (1. - prior) } else { (accuracy - prior) / prior }
}

pub trait ColumnIndex<Rhs> {
    type Output;
    fn get_columns(&self, index: &Rhs) -> Self::Output;
}

impl ColumnIndex<Vec<usize>> for Array {
    type Output = Array;
    fn get_columns(&self, index: &Vec<usize>) -> Array {
        let mut data = Vec::with_capacity(index.len() * self.rows());

        for &col_idx in index {
            for row_idx in 0..self.rows() {
                unsafe {
                    data.push(self.get_unchecked(row_idx, col_idx));
                }
            }
        }
        let mut result = Array::from(data);
        result.reshape(self.rows(), index.len());
        result
    }
}

pub fn smooth_usize(values: &[usize], window_size: usize) -> Vec<f32> {
    let mut result = Vec::with_capacity(values.len());
    for i in 0..values.len() - window_size {
        let window = &values[i..i + window_size];
        result.push(((window.iter().sum::<usize>() as f32) / (window_size as f32)).round());
    }
    result
}

pub fn build_histogram(values: &[f32], num_bins: usize) -> Vec<usize> {
    let mut counts = vec![0usize; num_bins];
    let max_v = values.iter().cloned().fold(1., f32::max);
    let min_v = values.iter().cloned().fold(0., f32::min);
    let n = (num_bins - 1) as f32;
    for v in values {
        let value = (v - min_v) / (max_v - min_v);  // normalize to unit
        counts[(value * n).round() as usize] += 1;
    }
    counts
}

pub fn guess_threshold(data: &[f32]) -> f32 {
    // FIXME: This is a *very* fragile way to find a local minimum, assuming there are two smooth peaks with no jagged sub-peaks
    let (mut p1, mut t1) = (0, 0);
    let (mut p2, mut t2) = (0, 0);
    let max = (data.iter().fold(0., |a: f32, b: &f32| a.max(*b)) + 1.).round() as usize;
    let (mut p3, mut t3) = (max, 0);
    for (i, v) in data.iter().map(|x| x.round() as usize).enumerate() {
        if v > p1 && p2 == 0 {  // increase p1 as long as v still increases and the second peak hasn't been found yet
            p1 = v;
            t1 = i;
        } else if p1 > 0 {  // if the first peak has been found already
            if v > p2 {
                p2 = v;
                t2 = i;
            }
        }
    }
    for i in t1..t2 {  // look for a minimum between the peaks
        let v = data[i].round() as usize;
        if v < p1 && v < p2 && v < p3 {
            p3 = v;
            t3 = i;
        }
    }
    t3 as f32 / data.len() as f32
}
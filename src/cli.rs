extern crate bio;
extern crate clap;
extern crate cue;
extern crate flate2;
extern crate itertools;
extern crate num_cpus;
extern crate pbr;
extern crate rust_htslib;
extern crate rustc_serialize;
extern crate rustlearn;

use bincode::deserialize_from;
use bincode::serialize_into;
use data::{SpaltCall, SpaltData, SpaltInfo};
use data::Model;
use lib::FeatureFn;
use lib::features::{end_proximity, is_read1, is_reverse, neighbour_base_quality};
use lib::PileupReaderSettings;
use lib::read_vcf_positions;
use lib::Region;
use lib::split2;
use self::clap::ArgMatches;
use self::flate2::bufread::GzDecoder;
use self::itertools::Itertools;
use self::rust_htslib::bam::HeaderView;
use std::fs::File;
use std::io;
use std::io::Write;
use std::str;
use std::sync::Arc;
use std::sync::Mutex;
use threshold::Threshold;

pub type SharedWriter = Arc<Mutex<Box<Write + Send>>>;

#[allow(unused_variables)]
pub fn write_vcf_entry(info: &SpaltInfo, data: Option<&SpaltData>, call: Option<&SpaltCall>, writer: &SharedWriter) {
//    use cli::rust_htslib::bcf;
//    use cli::rust_htslib::htslib;
//    let header = bcf::Header::new();
//    debug!("Header created");
//    let mut vcf_writer = bcf::Writer::from_stdout(&header, true, true).expect("Failed creating VCF Writer");
//    debug!("VCFWriter created");

    let mut lock = writer.lock().unwrap();
    let call = call.unwrap();

//    let mut record = vcf_writer.empty_record();
//    debug!("Empty record initialized.");
//    record.set_pos(info.ref_pos as i32);
//    record.set_qual(-10. * (-2. * 0.9999 * (0.5 - call.score).abs() + 1.).log10());
////    record.inner_mut().rid = info.tid as i32;
//    let mut alleles: Vec<Vec<u8>> = info.alt_bases.iter().map(|&c| vec![c as u8] ).collect();
//    alleles.insert(0, vec![info.ref_base as u8]);
//    debug!("{:?}", &alleles[..]);
//    record.set_alleles(&alleles[..]);
//    record.push_info("varType".as_bytes(), format!("{}", call.call).as_bytes(), htslib::BCF_HT_STR);
//    vcf_writer.write(&record);
//    use std::process::exit;
//    exit(0);
    let fdata = &data.expect("No data available").meta_features.feature_data;
    let mut msep: f32 = fdata.iter().sum();
    msep /= fdata.len() as f32;
    if (*lock).write_fmt(
        format_args!(
            "{}\t{}\t{}\t{}\t{}\t{:7.4}\t{}\tvarType={};MSep={};DP={};AAF={}\t{}",
            str::from_utf8(&info.tname).expect("Failed reading tname"), // CHROM
            info.ref_pos + 1, // POS
            ".", // ID
            info.ref_base as char, // REF
            info.alt_bases.iter().join(","),  // ALT
            -10. * (-2. * 0.9999 * (0.5 - call.score).abs() + 1.).log10(), // QUAL
            ".", // FILTER
            call.call, // INFO varType
            msep, // INFO +MSEP
            info.num_class1 + info.num_class2,
            info.num_class2 as f32 / (info.num_class1 as f32 + info.num_class2 as f32),
            "GT\t./." // FORMAT
        )).is_ok() {
        let _ = (*lock).write(b"\n");
    }
}

#[allow(unused_variables)]
pub fn write_data(info: &SpaltInfo, data: Option<&SpaltData>, call: Option<&SpaltCall>, writer: &SharedWriter) {
    let mut lock = writer.lock().unwrap();
    let data = data.unwrap();
    let sid: (SpaltInfo, SpaltData) = (info.clone(), data.clone());
    let result = serialize_into(&mut *lock, &sid);
    if result.is_err() {
        error!("Error serializing {:?}", &result);
        panic!("Error serializing {:?}", &result);
    } else {
        let _ = (*lock).flush();
    }
}

type Reader = Box<io::Read>;

#[inline]
fn read_spalt_result<R: ?Sized + io::Read>(reader: &mut R) -> Option<(SpaltInfo, SpaltData)> {
    let f: Result<(SpaltInfo, SpaltData), _> = deserialize_from(reader);
    if let Ok(v) = f {
        Some(v)
    } else { None }
}

pub struct SpaltReader {
    reader: Reader
}

impl SpaltReader {
    pub fn new(path: &str) -> SpaltReader {
        let reader: Reader = if path.ends_with(".gz") {
            Box::new(GzDecoder::new(io::BufReader::new(File::open(path).unwrap())))
        } else {
            Box::new(io::BufReader::new(File::open(path).unwrap()))
        };
        SpaltReader { reader }
    }
}

impl Iterator for SpaltReader {
    type Item = (SpaltInfo, SpaltData);
    fn next(&mut self) -> Option<Self::Item> {
        read_spalt_result(&mut self.reader)
    }
}

pub struct CommonArgs {
    pub inpath: String,
    pub outpath: String,
    pub num_io_worker: usize,
    pub num_worker: usize,
    pub settings: PileupReaderSettings,
    pub model: Model,
    pub classifiers: Vec<String>,
}

pub fn parse_common_args(matches: &ArgMatches) -> CommonArgs {
    let inpath = matches.value_of("INPUT").unwrap();  // required argument
    let outpath = matches.value_of("output").unwrap_or("stdout");

    let num_io_worker: usize = match matches.value_of("iojobs") {
        Some(i) => 1.max(i.parse().unwrap()),
        None => 1
    };

    let num_worker: usize = match matches.value_of("jobs") {
        Some(i) if i == "-1" || i == "0" => num_cpus::get() / num_io_worker + 1,
        Some(i) => i.parse().unwrap(),
        None => 1
    };

    let feature_fns: Vec<FeatureFn> = if let Some(m) = matches.values_of("feature_fns") {
        m.map(|name| match name {
            "is_reverse" => is_reverse,
            "is_read1" => is_read1,
            "neighbour_base_quality" => neighbour_base_quality,
            "end_proximity" => end_proximity,
            _ => panic!("No such function {}", name)
        }).collect()
    } else {
        panic!("Unable to parse function specification")
    };

    let classifiers = if let Some(c) = matches.values_of("classifiers") {
        c.map(|n| n.to_owned()).collect_vec()
    } else {
        vec!["lsvm".to_owned(), "rbfsvm".to_owned(), "dtree".to_owned()]
    };

    let min_read_depth: usize = matches.value_of("min_read_depth").unwrap().parse().unwrap_or(10);
    let min_alt_frac: f32 = matches.value_of("min_alt_frac").unwrap().parse().unwrap_or(0.1);

    let model: Model = match matches.value_of("model") {
        Some(path) if path.starts_with("threshold:") => {
            let v: Vec<_> = path.split(":").collect();
            let t: f32 = v[1].parse().expect("Failed parsing threshold.");
            Model::MeanThreshold(Box::new(Threshold::from_value(t)))
        }
        Some(path) => {
            let mut f = File::open(path).expect("File not found.");
            let mut s = String::new();
            use std::io::Read;
            f.read_to_string(&mut s).expect("Error reading file.");
            rustc_serialize::json::decode(&s).expect("Error loading model.")
        }
        _ => Model::MeanThreshold(Box::new(Threshold::from_value(0.8))),
    };

    use super::std::collections::HashSet;
    let skip: HashSet<(u32, u32)> = if let Some(path) = matches.value_of("exclude") {
        let rstate = super::std::collections::hash_map::RandomState::new();
        read_vcf_positions(path, &rstate)
    } else {
        HashSet::new()
    };
    CommonArgs {
        inpath: inpath.to_string(),
        outpath: outpath.to_string(),
        num_io_worker,
        num_worker,
        settings: PileupReaderSettings::new(min_alt_frac, min_read_depth, feature_fns, skip),
        model,
        classifiers,
    }
}

pub fn parse_regions(regionstrings: clap::Values, header: &HeaderView) -> Vec<Region> {
    regionstrings.map(|desc| {
        assert!(desc.contains(':'));
        assert!(desc.contains('-'));
        let (tname, range) = split2(desc, ':');
        let (start, end) = split2(range, '-');
        let (start, end): (u32, u32) = (start.parse().unwrap(), end.parse().unwrap());

        let (tname, chr_tname) =
            if tname.starts_with("chr") {
                (&tname[3..], tname.to_string())
            } else {
                (tname, format!("{}{}", "chr", tname))
            };
        let (tname, chr_tname) = (tname.as_bytes(), chr_tname.as_bytes());

        let tnames: Vec<&[u8]> = header.target_names();
        let (tname, tid) =
            if tnames.contains(&tname) {
                (tname, header.tid(tname))
            } else if tnames.contains(&chr_tname) {
                (chr_tname, header.tid(chr_tname))
            } else {
                (tname, None)
            };
        match tid {
            Some(tid) => {
                Region { tname: tname.to_vec(), tid, start, end }
            }
            _ => {
                panic!("Neither '{tname}' nor '{chr_tname}' were found in the header.",
                       tname = str::from_utf8(tname).unwrap(),
                       chr_tname = str::from_utf8(chr_tname).unwrap());
            }
        }
    }).collect::<Vec<Region>>()
}

pub fn _print_histogram(histogram: &[usize], threshold: usize, height: usize) {
    let height = height - 2;  // 1 line for axis labels, 1 line for the threshold marker
    let max = *histogram.iter().max().unwrap_or(&1);
    let histogram: Vec<usize> = histogram.iter().map(|&v| ((v as f32 / max as f32) * height as f32).round() as usize).collect();
    let num_bins = histogram.len();
    let mut plot = vec![vec![false; num_bins]; height];
    for row in (0..height).rev() {
        for col in 0..num_bins {
            if histogram[col] > row {
                if col < threshold {
                    print!("░");
                } else {
                    print!("▒");
                }
            } else { print!(" "); }
            plot[height - row - 1][col] = if histogram[col] >= row { true } else { false };
        }
        println!();
    }
    // mark threshold
    {
        for _ in 0..threshold - 1 {
            print!(" ")
        }
        print!("><");
        for _ in threshold..num_bins {
            print!(" ")
        }
    }
    println!();
    // print axis
    {
        for i in 0..num_bins {
            if i % 10 == 0 {
                let label = format!("{:02}", (i as f32 / num_bins as f32 * 100.).floor() as usize);
                print!("{}", label);
            } else if i % 10 == 1 {
                continue;
            } else {
                print!(" ");
            }
        }
        println!();
    }
}